import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:agclinic_front_web/componentes/business_entities/responses_http.dart';
import 'package:http/http.dart' as http;

import 'package:agclinic_front_web/componentes/business_entities/usuario.dart';


/// Repositório que aramazena os dados do usuário logado no sistema e seus métodos
class UsuarioRepositorio{

  final Usuario _usuario = Usuario.naoInicializado();

  /// Autentica o usuário no controle de acesso do sistema
  Future<http.Response> autenticacao(Usuario usuario) async{

    const clientID = 'com.local.test:mysecret';
    
    var body = 'username=' + usuario.email +  '&password='  + usuario.senha + '&grant_type=password';

    final String clientCredentials = 
        const Base64Encoder().convert('$clientID'.codeUnits);
    
    final http.Response response =
      await http.post('http://localhost:8888/auth/token',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic $clientCredentials'
        },
        body: body);

    return response;

  }

/// Autentica o usuário no controle de acesso do sistema
  Future<http.Response> resetSenhaTeste(Usuario usuario) async{

    const clientID = 'com.local.test:mysecret';
    
    var body = 'username:${usuario.email},password:${usuario.email}';

    final String clientCredentials = 
        const Base64Encoder().convert('$clientID'.codeUnits);
    
    final http.Response response =
      await http.post('http://localhost:8888/register',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic $clientCredentials'
        },
        body: body);

    return response;

  }


  /// Autentica o usuário no controle de acesso do sistema
  Future<http.Response> resetSenha(Usuario usuario) async{

    const clientID = 'com.local.test:mysecret';
    
    var body = json.encode({'username': usuario.email});

    final String clientCredentials = 
        const Base64Encoder().convert('$clientID'.codeUnits);
    
    final http.Response response =
      await http.post('http://localhost:8888/reset',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic $clientCredentials'
        },
        body: body);

    return response;

  }

  /// Exclui o token do usuário após realizar o logout
  Future<bool> tokenValido(String token) async{

   final http.Response response =
      await http.get('http://localhost:8888/me',
        headers: {
          'Authorization': 'Bearer $token'
        },
      );

    if (response != null){
      return true;
    }
    return false;
  }

  /// Exclui o token do usuário após realizar o logout
  Future<void> deletaToken() async{
    await Future.delayed(Duration(seconds: 1));
    return;    
  }

  /// Armazena na seção o token do usuário conectado
  Future<void> salvaToken(String access_token) async{
    _usuario.token = access_token;
    return;
  }

  /// Armazena na seção o token do usuário conectado
  Future<void> salvaAutenticacaoUsuario(Usuario usuario, ResponseTokenAutenticacao response) async{
    usuario.token = response.access_token;
    usuario.id = response.id;
    if (usuario.manterConectado){
      var usuarioJson = usuario.jsonString;
      window.localStorage['USUARIO_AGILE'] = usuarioJson;
    }
  }

  /// Remove os dados do usuário do localStorage
  Future<void> limpaAutenticacaoUsuario() async{
    if (window.localStorage['USUARIO_AGILE'] != null){
      window.localStorage.remove('USUARIO_AGILE');
    }
  }

  Future<bool> verificaEmail(String email){
    
  }

  ///
  Future<Usuario> verificaSeUsuarioPossuiTokenValidoSalvo() async{
    try{
      var usuarioLocalStorage = window.localStorage['USUARIO_AGILE'];
      if (usuarioLocalStorage != null){
        var usuario = Usuario.fromJSON(usuarioLocalStorage);
        var tokenValido =  await this.tokenValido(usuario.token);
        if (tokenValido != null){
          return Usuario.fromJSON(usuarioLocalStorage);
        } else {
          return null;  
        }
      }
      else {
        return null;
      }
    }catch(e){
      return null;
    }    
  }

  /// Verifica se o usuário possui um token válido
  Future<bool> possuiToken() async{
    await Future.delayed(Duration(seconds: 1));
    return false;
  }

  /// Retorna a instância do Singleton do objeto usuário
  Usuario getUsuario() {
    return Usuario.naoInicializado();
  }

  Future<bool> estaLogado() async{
    return true;
  }



}

