import 'package:angular_router/angular_router.dart';

import 'route_paths.dart';
import 'package:agclinic_front_web/componentes/login/login.template.dart' as login_template;
import 'package:agclinic_front_web/componentes/cadastro_login/cadastro_login.template.dart' as cadastrologin_template;
import 'package:agclinic_front_web/componentes/painel/painel.template.dart' as painel_template;


export 'route_paths.dart';

class Routes {
  static final login = RouteDefinition(
    routePath: RoutePaths.login, 
    component: login_template.LoginNgFactory,
    ///useAsDefault: true,   
  );

  static final cadastrologin = RouteDefinition(
    routePath: RoutePaths.cadastrologin,
    component: cadastrologin_template.CadastroLoginNgFactory,
    ///useAsDefault: true,   
  );  


  static final painel = RouteDefinition(
    routePath: RoutePaths.painel,
    component: painel_template.PainelNgFactory, 
  );

  static final all = <RouteDefinition>[
    login,
    painel,
    cadastrologin,
  ];
}