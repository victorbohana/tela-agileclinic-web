import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:agclinic_front_web/componentes/navbar/navbar.dart';
import 'package:agclinic_front_web/routes/routes.dart';
import 'package:agclinic_front_web/routes/route_paths.dart';

@Component(
  selector: 'painel',
  templateUrl: 'painel.html',
  styleUrls: ['painel.css'],
  directives: [coreDirectives, routerDirectives, formDirectives, Navbar],
)
class Painel implements OnInit, OnActivate {
  final Router _router;

  Painel(this._router);

  @override
  void ngOnInit() {}

  @override
  void onActivate(RouterState previous, RouterState current) {
    // TODO: implement onActivate
    var itens = document.getElementsByClassName('is-sidebar').forEach((e) {
      var element = e as Element;
      element.onMouseOut.listen(modificaSidebar);
      element.onMouseOver.listen(modificaSidebar);
    });
  }

  void modificaSidebar(Event e) {
    var nomeUsuario = document.getElementById('nome-usuario');
    var emailUsuario = document.getElementById('email-usuario');
    var sidebar = document.getElementById('sidebar-wrapper');
    var imagem = document.getElementById('logo-navbar') as ImageElement;
    if (e.type == 'mouseout') {
      sidebar.style.width = '8vw';
      shiftTextItemSidebar('esconde');
      imagem.src = '../..//web/img/logo-agclinic-simbolo.png';
      nomeUsuario.text = 'MA';
      emailUsuario.text = '@';
    } else if (e.type == 'mouseover') {
      sidebar.style.width = '15vw';
      shiftTextItemSidebar('mostra');
      imagem.src = '../..//web/img/logo-agileclinic-normal.png';
      nomeUsuario.text = 'Marcio Alencar';
      emailUsuario.text = 'marciotrix77@gmail.com';
    }
  }

  void shiftTextItemSidebar(String operacao) {
    var itens = document.getElementsByClassName('sidebar-title').forEach((e) {
      var element = e as Element;

      element.classes.remove('hide');
      //element.style.display = 'block';

      if (operacao == 'esconde') {
        element.classes.add('hide');
        //element.style.display = 'none';
      }
    });
  }
}
