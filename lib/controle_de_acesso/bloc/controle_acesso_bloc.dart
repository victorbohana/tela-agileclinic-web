// Bibliotecas externas
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:angular_router/angular_router.dart';

// Bibliotecas do projeto
import 'package:agclinic_front_web/componentes/business_entities/responses_http.dart';
import 'package:agclinic_front_web/componentes/business_entities/usuario.dart';
import 'package:agclinic_front_web/componentes/business_entities/usuario_login.dart';
import 'package:agclinic_front_web/controle_de_acesso/repositorio/usuarioRepositorio.dart';
import 'package:agclinic_front_web/routes/route_paths.dart';

// Parte da classe
part 'controle_acesso_evento.dart';
part 'controle_acesso_estado.dart';

/// Controlador dos acessos ao sistema e suas funcionalidades. 
/// 
/// Classe utiliza os padrões Singleton e BLOC para  ser acessada 
/// em única instância em todos os componentes(Singleton) e para 
/// separar a interface das regras de negócio (Bloc). 
class ControleAccessoBloc extends Bloc<ControleAcessoEvento, ControleAcessoEstado>{

  /// Usuário que está acessando o sistema
  var usuario = Usuario.naoInicializado();

  /// Controle do status dos campos do formulário de login e do retorno 
  /// das operações.
  var usuarioLogin = UsuarioLogin.naoInicializado();

  /// Rotas de acesso aos componentes do sistema
  Router _router;

  /// Construtor privado do padrão singleton
  ControleAccessoBloc._construtorPrivado();
  
  /// atributo de acesso a instância única
  static final ControleAccessoBloc _instancia = ControleAccessoBloc._construtorPrivado();

  /// Método que retorna instância única da classe
  static ControleAccessoBloc get instancia => _instancia;

  /// set para o objeto das rotas de acesso aos componentes do sistema.
  set router(Router router){
    _router=router;
  }

  /// get para o objeto das rotas de acesso aos componentes do sistema.
  Router get router{
    return _router;
  }

  /// Retorna o estado inicial do objeto
  @override
  get initialState => NaoInicializado(usuario, usuarioLogin);

  /// Método de entrada do objeto, chamado sempre pelo método add da 
  /// classe bloc. 
  @override
  Stream<ControleAcessoEstado> mapEventToState(event) async*{
    if (event is IniciarAplicacao){
      yield* _mapEstadoIniciarAplicacao(router);
    } else if (event is SolicitarLogin){
      yield* _mapEstadoSolicitarLogin(router, usuario);
    } else if (event is SolicitarLogout){
      yield* _mapEstadoSolicitaLogout(router, usuario);
    } else if (event is SolicitarResetSenha){
      yield* _mapEstadoSolicitaResetSenha(router, usuario);
    }
  }

  Stream<ControleAcessoEstado> _mapEstadoSolicitaResetSenha(Router router, Usuario usuario) async*{
    try{
      yield ResetandoSenha(usuario, UsuarioLogin.logando());
      var usuarioRepositorio = UsuarioRepositorio();
      var response = await usuarioRepositorio.resetSenha(usuario);  
      if (response.statusCode == 200){
        yield SenhaResetada(usuario, UsuarioLogin.senhaResetada());
      }else {
        yield ResetSenhaFalahado(usuario, UsuarioLogin.falhado('Email não cadastrado!'));
      }
    } catch(error){
      yield ResetSenhaFalahado(usuario, UsuarioLogin.falhado('Erro no acesso ao servidor!'));
    }     
  }

  /// Inicia a aplicação
  /// 
  /// Veririca se o usuário salvou sua conexão no último login. Caso afirmativo, 
  /// o sistema busca o token salvo e verifica se válido. Se sim, o usuário é 
  /// direcionado para o painel, caso negativo para a tela de login.
  Stream<ControleAcessoEstado> _mapEstadoIniciarAplicacao(Router router) async*{

    /// Envia o estado Logando.
    yield Logando(usuario, UsuarioLogin.logando());
   
    var usuarioRepositorio = UsuarioRepositorio();

    /// Usuário caso possua token salvo
    var usuarioStorage = await usuarioRepositorio.verificaSeUsuarioPossuiTokenValidoSalvo();

    /// Se usuário possui toke salvo, direciona para o painel. Caso negativo,
    /// direciona para o login.
    if (usuarioStorage != null){
      yield Logado(usuarioStorage,UsuarioLogin.logado());   
      await router.navigate(RoutePaths.painel.toUrl());   
    } else {
      yield Deslogado(usuario, UsuarioLogin.naoInicializado());
      await router.navigate(RoutePaths.login.toUrl());
    }
  }

  /// Executa procedimento de Login do usuário após envio do fomrulário
  /// 
  /// Retorna os seguintes estados no Stream:
  ///   [Logando] ao inciar a execução;
  ///   [Logado] quando o login for realizado com sucesso;
  ///   [Login Falhado] quando o login falar por digitação ou por erro no servidor.
  Stream<ControleAcessoEstado> _mapEstadoSolicitarLogin(Router router, Usuario usuario) async*{
    try{
      yield Logando(usuario, UsuarioLogin.logando());
      var usuarioRepositorio = UsuarioRepositorio();
      var response = await usuarioRepositorio.autenticacao(usuario);  
      if (response.statusCode == 200){
        var responseToken =  ResponseTokenAutenticacao.fromJson(json.decode(response.body));
        await usuarioRepositorio.salvaAutenticacaoUsuario(usuario,responseToken);
        yield Logado(usuario, UsuarioLogin.logado());
      }else {
        yield LoginFalhado(usuario, UsuarioLogin.falhado('Email e/ou senha inválidos!'));
      }
    } catch(error){
      yield LoginFalhado(usuario, UsuarioLogin.falhado('Erro no acesso ao servidor!'));
    }   
  }     
}

Stream<ControleAcessoEstado> _mapEstadoSolicitaLogout(Router router, Usuario usuario) async*{
  var usuarioRepositorio = UsuarioRepositorio();
  await usuarioRepositorio.limpaAutenticacaoUsuario();
  yield NaoInicializado(Usuario.naoInicializado(), UsuarioLogin.naoInicializado());
  await router.navigate(RoutePaths.login.toUrl());
}
