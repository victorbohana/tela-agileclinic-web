part of 'controle_acesso_bloc.dart';


abstract class ControleAcessoEstado extends Equatable{

  final Usuario _usuario;

  final UsuarioLogin _login;

  ControleAcessoEstado(this._usuario, this._login);

  @override
  List<Object> get props => [_usuario, _login];
  

}

class NaoInicializado extends ControleAcessoEstado{

  NaoInicializado(Usuario usuario, UsuarioLogin login) : super(usuario, login);

  @override
  List<Object> get props => [_usuario,_login];  
}

class Logando extends ControleAcessoEstado{
  
  Logando(Usuario usuario, UsuarioLogin login) : super(usuario, login);

  @override
  List<Object> get props => [_usuario,_login];  
}

class Logado extends ControleAcessoEstado{
  
  Logado(Usuario usuario, UsuarioLogin login) : super(usuario, login);

  @override
  List<Object> get props => [_usuario,_login];  
}

class LoginFalhado extends ControleAcessoEstado{
  
  LoginFalhado(Usuario usuario, UsuarioLogin login) : super(usuario, login);

  @override
  List<Object> get props => [_usuario,_login];  
}

class Deslogado extends ControleAcessoEstado{
  
  Deslogado(Usuario usuario, UsuarioLogin login) : super(usuario, login);

  @override
  List<Object> get props => [_usuario,_login];  
}

class SenhaResetada extends ControleAcessoEstado{
  
  SenhaResetada(Usuario usuario, UsuarioLogin login) : super(usuario, login);

  @override
  List<Object> get props => [_usuario,_login];  
}

class ResetandoSenha extends ControleAcessoEstado{
  
  ResetandoSenha(Usuario usuario, UsuarioLogin login) : super(usuario, login);

  @override
  List<Object> get props => [_usuario,_login];  
}

class ResetSenhaFalahado extends ControleAcessoEstado{
  
  ResetSenhaFalahado(Usuario usuario, UsuarioLogin login) : super(usuario, login);

  @override
  List<Object> get props => [_usuario,_login];  
}