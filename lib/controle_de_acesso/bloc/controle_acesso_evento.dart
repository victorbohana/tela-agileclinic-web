part of 'controle_acesso_bloc.dart';

abstract class ControleAcessoEvento extends Equatable{
  const ControleAcessoEvento();

  @override
  List<Object> get props => [];

}

class IniciarAplicacao extends ControleAcessoEvento{

  @override
  List<Object> get props => [];

}

class SolicitarLogin extends ControleAcessoEvento{

  @override
  List<Object> get props => [];

}

class SolicitarLogout extends ControleAcessoEvento{

  @override
  List<Object> get props => [];

}

class SolicitarResetSenha extends ControleAcessoEvento{

  @override
  List<Object> get props => [];

}

