import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:agclinic_front_web/app_component.template.dart' as ng;
import 'package:agclinic_front_web/componentes/business_entities/usuario.dart';
import 'main.template.dart' as self;

@GenerateInjector([
  routerProvidersHash, // You can use routerProviders in production
])
final InjectorFactory injector = self.injector$Injector;

void main() {

  // Objeto singleton que armazenará todos os dados do Usuario que acessa a aplicação
  Usuario usr = Usuario.naoInicializado();

  runApp(ng.AppComponentNgFactory, createInjector: injector);
}
